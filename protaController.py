from threading import Thread
import dill
import struct
import time
from prota import Prota

class MasterProtaController:
	def __init__(self,sc):
		self.sc= sc
		self.processDict={}
		self.runningProcesses=[]
		self.alive=True
		self.nProtas=0

	def start(self):
		send_thread=Thread(target=self.__sendLoop)
		send_thread.setDaemon(True)
		send_thread.start()
		recv_thread=Thread(target=self.__recvLoop)
		recv_thread.setDaemon(True)
		recv_thread.start()

	def __sendLoop(self):
		while(self.alive):    
			processDict=self.processDict.copy()
			for pid in processDict:
				if (pid not in self.runningProcesses and pid in self.processDict):
					if(not pid%2):
						self.__sendProta(pid)
					else:
						self.__sendMethod(pid)

					self.runningProcesses.append(pid)

	def __recvLoop(self):
		while(self.alive):
			try:
				data=self.sc.recv(8)
				(pid, l)=struct.unpack(">2I",data)
				data=bytearray()
				while(len(data)<l):
					packet=self.sc.recv(l-len(data))
					data.extend(packet)

				result=dill.loads(data)
				if(not pid%2):
					self.processDict[pid].setResult(result)
					self.processDict.pop(pid)
					self.runningProcesses.remove(pid)
					self.nProtas-=1
				else:
					self.processDict[pid-1].setMethodResult(result)
					self.processDict.pop(pid)
					self.runningProcesses.remove(pid)
			except:
				pass

	def __sendProta(self, pid):
		if(pid):
			to_send=dill.dumps(self.processDict[pid].getRunnable())
		else:
			to_send=dill.dumps(None)

		l=struct.pack(">2I",pid,len(to_send))
		self.sc.send(l)
		self.sc.send(to_send)

	def __sendMethod(self, pid):
		to_send=dill.dumps(self.processDict[pid])
		l=struct.pack(">2I",pid,len(to_send))
		self.sc.send(l)
		self.sc.send(to_send)

	def createProta(self, prota):
		i=2
		while( i in self.processDict):
			i+=2

		self.pid=i
		prota.setPid(i)
		prota.assignController(self)
		self.processDict[i]=prota
		self.nProtas+=1

	def runProtaMethod(self, fun, pid):
		self.processDict[pid+1]=fun

	def isAlive(self):
		return self.alive

	def getNProtas(self):
		return self.nProtas

	def close(self):
		self.processDict[0]=None
		time.sleep(0.1)
		self.alive= False

class SlaveProtaController:
	def __init__(self,sc):
		self.sc= sc
		self.processDict={}
		self.alive=True

	def start(self):
		send_thread=Thread(target=self.__sendLoop)
		send_thread.setDaemon(True)
		send_thread.start()
		recv_thread=Thread(target=self.__recvLoop)
		recv_thread.setDaemon(True)
		recv_thread.start()

	def __sendLoop(self):
		while(self.alive):
			pid=0
			processDict=self.processDict.copy()
			for pid in processDict:
				if processDict[pid].isDone():
					self.__sendResult(pid)
					self.processDict.pop(pid)
				elif processDict[pid].isMethodDone():
					self.__sendMethodResult(pid)


	def __recvLoop(self):
		while(self.alive):
			try:
				data=self.sc.recv(8)
				(pid, l)=struct.unpack(">2I",data)
				if(pid):
					data=bytearray()
					while(len(data)<l):
						packet=self.sc.recv(l-len(data))
						data.extend(packet)

					runnable=dill.loads(data)
					if(not pid%2):
						self.processDict[pid]=Prota(runnable)
						self.processDict[pid].start()
					else:
						self.processDict[pid-1].callMethod(runnable)
				else:
					self.alive= False

			except:
				pass
			

	def __sendResult(self, pid):
		to_send=dill.dumps(self.processDict[pid].getResult())
		l=struct.pack(">2I",pid,len(to_send))
		self.sc.send(l)
		self.sc.send(to_send)

	def __sendMethodResult(self, pid):
		to_send=dill.dumps(self.processDict[pid].getMethodResult())
		l=struct.pack(">2I",pid+1,len(to_send))
		self.sc.send(l)
		self.sc.send(to_send)

	def isAlive(self):
		return self.alive

class ProtaMultiController(): #ProtaMultiController(Thread)
	def __init__(self):
		#Thread.__init__(self)
		self.controllerList=[]
		#self.setDaemon(True)
		#self.alive = True

	"""def run(self): #will be implemented in the future, if necessary
		while self.alive:
			pass"""
	
	def addController(self, controller):
		self.controllerList.append(controller)

	def createProta(self, prota):
		nProtas=[]
		for controller in self.controllerList:
			nProtas.append(controller.getNProtas())
		
		el=nProtas.index(min(nProtas))
		self.controllerList[el].createProta(prota)
	def startAll(self):
		for controller in self.controllerList:
			controller.start()

	def closeall(self):
		#self.alive= False
		#time.sleep(0.1)
		for controller in self.controllerList:
			controller.close()