import socket
import sys
from protaController import SlaveProtaController

so=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
so.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
so.bind(("127.0.0.1",int(sys.argv[1])))
so.listen(1)
while 1:
    sc, addr= so.accept()
    controller=SlaveProtaController(sc)
    controller.start()
    while controller.isAlive():
        pass
    sc.close()