from threading import Thread

class Prota:
    def __init__(self, runnable):
        self.runnable= runnable
        self.result=None
        self.done= False
        self.pid= None
        self.methodMsDone=True
        self.methodSlDone=False
        self.methodResult= None
        self.fun= None
        self.controller= None

    def setPid(self, pid):
        self.pid= pid

    def getPid(self):
        return self.pid

    def start(self):
        thread=Thread(target=self.run)
        thread.setDaemon(True)
        thread.start()

    def run(self):
        try:
            self.result=self.runnable.run()
        except Exception as e:
            self.result=e

        self.done= True

    def getResult(self):
        return self.result

    def isDone(self):
        return self.done

    def setResult(self, result):
        self.result= result
        self.done= True

    def getRunnable(self):
        return self.runnable

    def waitResult(self):
        while (not self.done):
            pass
        return self.result

    def isRunning(self):
        return not self.done

    def callMethod(self, fun):
        self.methodSlDone= False
        self.fun= fun
        thread=Thread(target=self.runMethod())
        thread.setDaemon(True)
        thread.start()

    
    def runMethod(self):
        try:
            funName= self.fun[0]
            args=self.fun[1]
            attr=getattr(self.runnable, funName)
            self.methodResult=attr(*args)
        except Exception as e:
            self.methodResult= e

        self.methodSlDone= True

    def callRemoteMethod(self, fun, args=[]):
        if(self.methodMsDone):
            self.methodMsDone= False
            funName=fun.__name__
            self.controller.runProtaMethod([funName, args], self.pid)

    def isMethodDone(self):
        return self.methodSlDone

    def setMethodResult(self, result):
        self.methodResult=result
        self.methodMsDone= True

    def getMethodResult(self):
        self.methodSlDone=False
        return self.methodResult

    def isMethodRunning(self):
        return not self.methodMsDone

    def waitMethodResult(self):
        while(not self.methodMsDone):
            pass

        return self.methodResult

    def assignController(self, controller):
        self.controller= controller