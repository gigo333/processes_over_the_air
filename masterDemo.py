import socket

from prota import Prota
from protaController import MasterProtaController
from protaController import ProtaMultiController

import time
class Dummy:
    np = __import__('numpy')
    #time=__import__('time')
    def __init__(self, a, b):
        self.a=a
        self.b=b

    def run(self):
        print("starting",self.b)
        #self.time.sleep(5)
        return [float(self.np.pi*self.a/self.b), self.a+self.b]

class Demo:
    time=__import__('time')
    def __init__(self, a, b):
        self.a=a
        self.b=b
        self.done= False

    def run(self):
        while(not self.done):
            pass

        self.time.sleep(0.05)
        return self.a**int(self.b)

    def fun(self):
        print("working")
        self.done= True
        return "done"


n_protas=10
n_slaves=1
scList=[]
for i in range(n_slaves):
    sc=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    sc.connect(("127.0.0.1", 1234+i))
    scList.append(sc)

multiController=ProtaMultiController()

for sc in scList:
    controller = MasterProtaController(sc)
    multiController.addController(controller)

multiController.startAll()
prota=[]
for i in range(n_protas):
    prota.append(Prota(Dummy(2,i)))
    multiController.createProta(prota[-1])

for i in range(n_protas):
    r=prota[i].waitResult()
    print(r)

#time.sleep(1)
prota1= Prota(Demo(2,8))
multiController.createProta(prota1)
prota1.callRemoteMethod(prota1.getRunnable().fun)
print(prota1.waitMethodResult())
result1=prota1.waitResult()
print(result1)

multiController.closeall()